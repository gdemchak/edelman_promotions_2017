import Component from 'inferno-component'
import paper from 'paper'
import splatter1 from '../assets/splatter/splatter1'
// var paper = require('paper');

class Canvas extends Component {

	constructor(props){
		super(props)
		this.values = {
			paths: 50,
			minPoints: 5,
			maxPoints: 15,
			minRadius: 30,
			maxRadius: 90
		};
	}

	componentDidMount() {


		var canvas = document.getElementById('canvas')
		paper.setup(canvas)
		var path = new paper.Path()

		path.strokeColor = 'white'
		var start = new paper.Point(100,100)
		path.moveTo(start)
		path.lineTo(start.add([200,-50]))
		paper.view.draw()

		
	// 	resizeCanvas();
		this.createPaths()
		paper.view.draw()
	}

	createPaths(){
		
		var values = this.values;
		var radiusDelta = values.maxRadius - values.minRadius;
		var pointsDelta = values.maxPoints - values.minPoints;
		var item = new paper.Symbol(paper.project.importSVG(splatter1, {insert: true}))
		
		
		for (let i = 0; i < this.values.paths; i++) {

			// let circle = new paper.Path.Circle(new paper.Point(i + 10,i+10), 50)
			// circle.fillColor = 'white';

			let radius = values.minRadius + Math.random() * radiusDelta;
			let points = values.minPoints + Math.floor(Math.random() * pointsDelta);
			// var path = this.createBlob(paper.view.size * paper.Point.random(), radius, points);
			let path = new paper.Path();
			path.closed = true;
			for (let i = 0; i < points; i++) {
				let delta = new paper.Point({
					length: (radius * 0.5) + (Math.random() * radius * 0.5),
					angle: (360 / points) * i
				});
				path.add( (paper.view.size * paper.Point.random()) + delta );
			}
			console.log(path)
			path.smooth();
			let lightness = (Math.random() - 0.5) * 0.4 + 0.4;
			let hue = Math.random() * 360;
			path.fillColor = { hue: hue, saturation: 1, lightness: lightness };
			path.strokeColor = 'black';

			// paper.view.draw()
		};
	}

	createBlob(center, maxRadius, points) {
		var path = new paper.Path();
		path.closed = true;
		for (var i = 0; i < points; i++) {
			var delta = new paper.Point({
				length: (maxRadius * 0.5) + (Math.random() * maxRadius * 0.5),
				angle: (360 / points) * i
			});
			path.add(center + delta);
		}
		path.smooth();
		console.log(path)
		return path;
	}

	render() {

		return (
    		<div></div>
		)

	}
}

export default Canvas