import Component from 'inferno-component'

class Splatters extends Component {

	componentDidMount(){

	}


	render(){
		
		return (
			<div id="splatter-container" className="container">
				<img id="splatter_1" className="splatter" src={process.env.PUBLIC_URL + "assets/splatters/Object1.png"} alt="splatter"/>
				<img id="splatter_2" className="splatter" src={process.env.PUBLIC_URL + "assets/splatters/Object2.png"} alt="splatter"/>
				<img id="splatter_3" className="splatter" src={process.env.PUBLIC_URL + "assets/splatters/Object3.png"} alt="splatter"/>
				<img id="splatter_4" className="splatter" src={process.env.PUBLIC_URL + "assets/splatters/Object4.png"} alt="splatter"/>
				<img id="splatter_5" className="splatter" src={process.env.PUBLIC_URL + "assets/splatters/Object5.png"} alt="splatter"/>
			</div>
		)

	}
}

export default Splatters