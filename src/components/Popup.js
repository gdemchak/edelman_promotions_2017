import Component from 'inferno-component'



class Popup extends Component {


	handleClose() {
		var popup = document.getElementById('popup')
		var popup_container = document.getElementById('popup-container')
		document.getElementById('popup-image').src = '#'
		
		popup_container.style.opacity = 0
		
		popup_container.className = 'overlay hidden'
		popup.className = 'overlay hidden'
		
	}

	render() {

		return (
			<div id="popup" className="overlay hidden">
				<div id="popup-close">
						<img id="popup-button" onClick={ () => {this.handleClose() } } src={process.env.PUBLIC_URL + `./assets/close_button.png`}/>
				</div>
				<div id="popup-content-container">
					<div>
					<img id="popup-image" alt="profile" src="#" />
					</div>
					<div id="popup-text-container">
						<h3 id="popup-name"></h3>
						<h4 id="popup-title"></h4>
						<p id="popup-blurb"></p>
					</div>
				</div>
			</div>
		)

	}
}

export default Popup