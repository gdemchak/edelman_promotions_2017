import Component from 'inferno-component'

class Employee extends Component {
	constructor(props){
		super(props)
		this.state = {
			display_width: this.props.display_width
		}
	}

	handleClick(data){
		console.log(data)
		var name = document.getElementById('popup-name');
		var title = document.getElementById('popup-title');
		var blurb = document.getElementById('popup-blurb');
		var popup_container = document.getElementById('popup-container');
		var popup = document.getElementById('popup');

		var image = document.getElementById('popup-image');

		name.innerHTML = data.first_name.toUpperCase() + ' ' + data.last_name.toUpperCase()
		title.innerHTML = data.promotion_title;
		blurb.innerHTML = data.promo_blurb;
		image.src = `${process.env.PUBLIC_URL}/assets/profiles/${this.underline(data.last_name)}_${this.underline(data.first_name)}_color.jpg`;

		
		popup_container.style.opacity = 0.5;
		popup_container.className = 'overlay visible'
		popup.className = 'overlay visible'


	}


	underline(name){
		return name.split(' ').join('_')
	}

	render({employee}) {
		
		return (
			<li className="employee" onClick={() => {this.handleClick(employee)}}>
				<div id={employee.first_name.toLowerCase().split(/[&\/\\#,+()$~%.'":*?<>{} ]/g).join('_') + '_' + employee.last_name.toLowerCase().split(/[&\/\\#,+()$~%.'":*?<>{} ]/g).join('_')} className="profile-container">
					<img className="profile" src={`${process.env.PUBLIC_URL}/assets/profiles/${this.underline(employee.last_name)}_${this.underline(employee.first_name)}_color.jpg`} alt={`${employee.first_name}'s profile`}/>
				</div>	
				<h4 className="employee-name">{employee.first_name.toUpperCase()}<br/>{employee.last_name.toUpperCase()}</h4>
			</li>
		)
	}
}

export default Employee