import $ from 'jquery'
import sparkles from './jquery-canvas-sparkles.js'


window.onload = () => {

  loadSparklesPlugin();

  //do animations here
  console.log('begin animatons!')
  var edelman_logo = document.getElementById('edelman-logo');
  var level_up = document.getElementById('level-up');
  var title = document.getElementById('title');
  var description = document.getElementById('description');
  var employees = document.getElementById('employees');
  var app_body = document.getElementById('App-body');
  var background_image = document.getElementById('background_image');

  
    edelman_logo.style.transform = 'translateY(0px)'
    edelman_logo.style.opacity = 1
    
    level_up.style.transform = 'translateY(0px)'

    title.style.transform = 'translateX(0px)'
    title.style.opacity = 1

    description.style.transform = 'translateX(0px)'
    description.style.opacity = 1

    // background_image.style.opacity = 1;
  

  
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    //dont display hover effect on mobile devices.
    setTimeout( () => {
      app_body.style.opacity = 1;
    })
  } else {
    setTimeout( () => {
      $(".profile-container").sparkle({
      color: ['#C443FB',"#96F5F8","#FFFD39","#FFF","#F2311E"],
      minSize: 2,
      maxSize: 3,
      speed: 6,
      count: 800
    });
      app_body.style.opacity = 1;
    }, 1000)
  }

  setTimeout( () => {
    employees.style.transform = 'translateY(0px)'
    employees.style.opacity = 1
  },1000)

}

var loadSparklesPlugin = () => {
	$.fn.sparkle = sparkles
}