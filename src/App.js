import logo from './assets/Design/Email_Header/logo.png';
import level_up from './assets/Design/Email_Header/Level_Up.png';
import Component from 'inferno-component';
import Employee from './components/Employee.js';
import Popup from './components/Popup.js';
import Splatters from './components/Splatters'
import './registerServiceWorker';
import './App.css';
import './animations/animations';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      employees: [
        {
          "last_name": "Abolafia",
          "first_name": "Dylan",
          "promotion_title": "Account Executive",
          "promo_blurb": "At some point in the past year, I became the food waste guy, but I definitely do more. From supporting Bloomberg Philanthropies’ Mayors Challenge and Chobani products launches, to maintaining my streak placing op-eds for the Merchant Marine Academy, to yes, working on food waste projects, I’ve learned lots at Edelman, and I’m excited for year two."
        },
        {
          "last_name": "Albright",
          "first_name": "Samantha",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I’ve become an integral member of the Dove Men+Care team, helping guide integrated strategy and execution for the personal wash category. How do you launch new nature-inspired grooming products with a relevant cool factor? We built a fully-functioning treehouse inspired by our products that media, influencers, and clients all raved about."
        },
        {
          "last_name": "Allen",
          "first_name": "Caitlin",
          "promotion_title": "Executive Vice President, GCRM",
          "promo_blurb": "Everyone has an eBay story, and it’s energizing to work on a brand that’s part of the cultural zeitgeist. I’m proud of the passion we demonstrate every day – and that our cross-practice, cross-office team continually pushes the boundaries of Communications Marketing. We won’t rest until people finally think of eBay as a modern marketplace, NOT the world’s largest garage sale."
        },
        {
          "last_name": "Alman",
          "first_name": "Lee",
          "promotion_title": "Executive Vice President",
          "promo_blurb": "In 5+ years here, I’ve had the chance to lead truly impactful work, like helping The Rockefeller Foundation in their fight against food waste; introducing Lambda Legal’s new CEO; and developing S&P’s first major ad campaign. I’ve learned something new every day since joining Edelman, from colleagues and clients alike."
        },
        {
          "last_name": "Barkley",
          "first_name": "Callie",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "This year, I contributed to two successful product launches for Well Yes! Soup and the introduction of Chobani’s first non-Greek yogurt. I am most proud to have helped surpass the Chobani client ask of one media placement per week, securing stories in top outlets such as Refinery29, People and Delish."
        },
        {
          "last_name": "Barrett",
          "first_name": "Shannon",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "Working across the office with so many different teams and amazing people to help advance our business. From robots to women’s reproductive rights and everything in between, its been an inspirational year chasing  business."
        },
        {
          "last_name": "Benko",
          "first_name": "Ciara",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "It’s been a busy year, but one of the things I’m most thrilled about is launching Homesense, HomeGoods’ new sister store. Our tiny but mighty team raked in (literally) a billion impressions, but my proudest moment may be my awkward photobomb in our Cosmo video."
        },
        {
          "last_name": "Bergl",
          "first_name": "Skylar",
          "promotion_title": "Senior Account Supervisor, Senior Editor",
          "promo_blurb": "My last year at Edelman, I edited three issues of Intelligence magazine, helped tell the story The Home Depot and Mitsubishi Regional Jet, gave voice to NBA player J.J. Redick, ESPN personality Mike Greenberg, and Dos Equis’ New Most Interesting Man in The World."
        },
        {
          "last_name": "Biaggi",
          "first_name": "Alexis",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "Since starting at Edelman just over three years ago, I have immersed myself in a variety of health accounts, but in the past year, contributing to integrated work across offices, specialties and practices on the Shire Ophthalmics and NY Health new business teams has led to new dimensions of professional and personal growth."
        },
        {
          "last_name": "Bilton",
          "first_name": "Julianna",
          "promotion_title": "Account Executive",
          "promo_blurb": "Working on the opening of Kellogg’s NYC is one of the projects I’m most proud of. Throughout the launch, I gained experience booking talent interviews, securing articles with influential outlets, and began developing relationships with food and lifestyle media. These relationships have led to additional coverage across the Kellogg’s portfolio."
        },
        {
          "last_name": "Borland",
          "first_name": "Jen",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "This year, I’m proud of revamping the media mavens program on eBay – leading the team to turn relationship building into resulting top-tier coverage. In particular, I used insights gleaned from media conversations to develop a new approach to the brand’s spring cleaning storytelling – securing key coverage and extending the news cycle."
        },
        {
          "last_name": "Boyce",
          "first_name": "Ashia",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "This year, I’ve proven to be a solid resource for my clients and account teams. For a recent data milestone, I supported planning and execution of a multi-pronged approach to communicate data internally and externally despite heavy corporate news flow. I am excited to take on this new role!"
        },
        {
          "last_name": "Braunstein",
          "first_name": "Sarah",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "It's been a great year on NY’s Financial Communications team. I am proud of myself because I increased my responsibilities on all my retainer accounts and have taken the lead of several special situation accounts including regulatory actions with the DOJ, SEC and CFPB."
        },
        {
          "last_name": "Brigandi",
          "first_name": "Dave",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "Over the past year, I brought strategic and creative thinking to Dos Equis and Campbell’s Chunky, taking them to new levels in the men’s lifestyle and sports space. Whether evolving The Most Interesting Man campaign or booking media days with athletes/celebrities, we consistency led Edelman to break benchmarks and push the business forward."
        },
        {
          "last_name": "Butler",
          "first_name": "Alexandra",
          "promotion_title": "SAE, Stratey & Insights",
          "promo_blurb": "I am grateful to my managers that allowed me to grow and challenge myself strategically this year. I am proud of my work owning reports for Samsung Mobile, most notably leading the Note8 Launch as lead analyst, and challenging myself to deliver meaningful insights to the team."
        },
        {
          "last_name": "Camacho",
          "first_name": "Ashley",
          "promotion_title": "Finance Manager",
          "promo_blurb": "I am proud to say that I have been the day to day financial support for Creative Network the past year and continued to build and grow CN (now on year 3, woohoo) financial processes and reporting. I also assisted in the successful implementation of our new FY18 CN finance structure."
        },
        {
          "last_name": "Camardo",
          "first_name": "Cecilia",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I’m proud to do meaningful work for an iconic brand and for clients who are true partners. A standout moment for me this year was launching the cultural juggernaut that was the Starbucks Unicorn Frappuccino – I’ll never forget managing a news cycle that was unlike any other!"
        },
        {
          "last_name": "Cancelosi",
          "first_name": "Maggie",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "Year three of Edelman afforded me the opportunity to work with an impressive roster of colleagues across five practices – elevating my understanding of communications marketing and encouraging me to bring strategic and creative thinking across numerous brand campaign launches, product introductions, media/influencer events, spokesperson press days and earned media strategies for Chobani, SIX:02 and Campbell Soup Company. Here’s to another year of learning!"
        },
        {
          "last_name": "Cannellos",
          "first_name": "Lisa",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "This year, I’m proud of the collaborative work we’ve done on HomeGoods to seamlessly integrate earned PR with owned social for our clients; growing the business in the process. On Heineken, we’ve secured consistent quality coverage and plan to launch the brands’ first-ever new ‘wild lager’ beer next week – cheers to that!"
        },
        {
          "last_name": "Cardenas",
          "first_name": "Lina",
          "promotion_title": "Account Executive",
          "promo_blurb": "I was most proud to hit the ground running and deepen my healthcare industry knowledge, diving into the pharma, hospital and policy space. Highlights include developing several thought leadership plans for high profile executives, and supporting Johnson & Johnson at this year’s World Economic Forum."
        },
        {
          "last_name": "Carroll",
          "first_name": "Tyler",
          "promotion_title": "Account Executive",
          "promo_blurb": "Over the past year I’ve provided support on a wide range of digital projects. From my work on AstraZeneca’s Lungprint to the KIND Social Experiment, I’ve learned a lot about what it takes to bring big ideas to small devices."
        },
        {
          "last_name": "Castillo",
          "first_name": "Alexandra",
          "promotion_title": "Account Executive",
          "promo_blurb": "Having worked with many different teams and offices within Edelman has truly been an exciting and rewarding experience. One of the most fun projects I have worked on was a launch event for Dove Deodorant’s newest innovation – media were beyond impressed, and it led to quality pieces of earned coverage along with newly established relationships."
        },
        {
          "last_name": "Chazotte",
          "first_name": "Peter",
          "promotion_title": "Senior Service Delivery Specialist",
          "promo_blurb": "Efficiency and style, in that order. Function, then form!  Work is a series of puzzles. After solving them, you can only solve them faster. We are a service team, and customer satisfaction ranks high on the list.  Automating the “Profile Setup” procedures, has reduced 20 minutes of work down to 10."
        },
        {
          "last_name": "Chen",
          "first_name": "Laurin",
          "promotion_title": "Finance Manager",
          "promo_blurb": "In the spirit of “the relentless pursuit of excellence,” I am pleased to have put in place various best practices the past year."
        },
        {
          "last_name": "Chesler",
          "first_name": "Larry",
          "promotion_title": "Senior Vice President, Contracts Management",
          "promo_blurb": "Starting with Edelman New York’s Health and Consumer divisions, I very much enjoyed rolling out question and answer contract training sessions to obtain a better understanding of contracting issues of key concern to the account staff. We have found the level of audience participation in these sessions to be very good. We will continue to conduct these training  for other departments throughout this year and into 2018."
        },
        {
          "last_name": "Coll",
          "first_name": "Alex",
          "promotion_title": "HR Manager",
          "promo_blurb": "This year I'm proud of achieving my SPHR certification and conducting orientation for approximately 80+ new hires.  I also enjoyed hosting our first DiscoverEe Germany program, here in NY!"
        },
        {
          "last_name": "Collard",
          "first_name": "Emily",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I’m most proud of how I’ve grown as a client leader – serving as their go-to contact, liaising with our stellar Edelman colleagues. It’s been a crazy year, but it’s been awesome to see us come together as a team to push the boundaries of traditional PR."
        },
        {
          "last_name": "Couvillion",
          "first_name": "Monica",
          "promotion_title": "Account Executive",
          "promo_blurb": "I supported Samsung through some of the most highly-publicized crises in the consumer and corporate spaces this year, and stepped up to manage the team’s media monitoring capabilities. I also worked closely with ARRIS to establish a proactive and efficient client relationship while navigating numerous crises and projects."
        },
        {
          "last_name": "Crain",
          "first_name": "Will",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "I’m proud of the passionate team of individuals I’m fortunate to lead who dedicate their time, energy and creativity to positively impacting eBay’s business and brand."
        },
        {
          "last_name": "Cummings",
          "first_name": "Makena",
          "promotion_title": "Account Executive",
          "promo_blurb": "I am so grateful for the incredible opportunities I’ve had since working at Edelman. One of my favorites was coordinating a surprise visit from Aly Raisman at the Wendy Hilliard Gymnastics Foundation. Seeing the excitement as young, aspiring gymnasts met one of their idols was such a rewarding experience!"
        },
        {
          "last_name": "Deixler",
          "first_name": "Hannah",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "Working with new clients like Bloomberg Philanthropies, Shearman & Sterling, and The Rockefeller Foundation challenged me to develop new skills, connect with unfamiliar faces across the network, and become proficient in languages (hello, food waste!) I previously couldn’t speak. I’m keeping my eyes – and mind – open and letting my curiosity continue to lead me to new and exciting places this year."
        },
        {
          "last_name": "DeSimone",
          "first_name": "Lauren",
          "promotion_title": "Vice President",
          "promo_blurb": "This past year was exciting – I was part of the team who launched the Notes to Remember campaign, a true communications marketing campaign, offering the chance to work with many talented people across the network. I also continued many successful projects on AZ and recently joined the SPINRAZA team; looking forward to another exciting fiscal!"
        },
        {
          "last_name": "Doyle",
          "first_name": "Jack",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I joined Edelman as an intern and am proud to have contributed to work such as eBay’s holiday campaigns or helping Samsung Mobile get the word out about the Note8, building on my role as a media expert to become a trusted advisor and strategist."
        },
        {
          "last_name": "Dratch",
          "first_name": "Gabby",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I am especially proud of my ongoing work with the American Lung Association’s LUNG FORCE Initiative. In May, our team launched the #ShowYourLUNGFORCE social campaign which rallied American’s to join the fight against lung cancer – the #1 cancer killer of men and women in our country."
        },
        {
          "last_name": "Dugdale",
          "first_name": "Lauren",
          "promotion_title": "Executive Vice President",
          "promo_blurb": "Samsung saw plenty of disruption this year, including major shifts in leadership and major products aflame.  Through ups/downs/sideways moments, it’s been a pleasure leading the business at a pivotal time.  I’m most proud of how the team weathered this disruption together—staying incurably committed to the client and consumers, alike.  I’m blessed to work alongside such incredible humans every day."
        },
        {
          "last_name": "Entwistle",
          "first_name": "Juli",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "As a leader on the AstraZeneca Global CVMD business, I’m most proud of the worldwide launch of the CVMD narrative. An Edelman cross-functional success story, the narrative is being used by employees in more than 30 countries and the content has been downloaded 3,000+ times."
        },
        {
          "last_name": "Farnum",
          "first_name": "Julia",
          "promotion_title": "Account Supervisor, Digital Health",
          "promo_blurb": "I’m proud to have supported various corporate health projects. I've helped increase the visibility of executives on LinkedIn and establish a client’s corporate social media presence. Working to launch a microsite in six weeks as well as working with digital influencers were also huge achievements. Super excited for 2018!"
        },
        {
          "last_name": "Feleke",
          "first_name": "Sophia",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "This year has been an amazing challenge, taking some not-so-easily-talked-about clients to new heights. Whether launching the next high-end toilet with TOTO or reminding everyone at Pride to #ProtectYourWang with Trojan, it’s been great to push our clients to take risks that resonated positively with consumers."
        },
        {
          "last_name": "Fernandez",
          "first_name": "Patti",
          "promotion_title": "Account Executive",
          "promo_blurb": "Shire has always been a collaborative account, and I feel that our ability to work as a team to solve problems has only continued to help us grow. Since joining the account, I have worked across teams to streamline and update our creative process. I strive to provide helpful feedback and engage in a constructive way to ensure all client deliverables are crisp."
        },
        {
          "last_name": "FitzGerald",
          "first_name": "Morgan",
          "promotion_title": "Project Manager",
          "promo_blurb": "This was my first/best year at Edelman. I’ve loved becoming the main Digital PM for Citi and working with lovely teams on all types of projects."
        },
        {
          "last_name": "Fitzsimmons",
          "first_name": "Erin",
          "promotion_title": "Producer",
          "promo_blurb": "From my work on the Home Depot Spring Hiring videos, to Ellevest’s badass launch video, I feel proud and privileged to be a part of so many amazing projects alongside my amazing team. Thanks for allowing me to bring all my passions together for Edelman’s own remake of Dirty Dancing!"
        },
        {
          "last_name": "Galilei",
          "first_name": "Luis",
          "promotion_title": "Account Executive",
          "promo_blurb": "I’m proud to be a part of the AXE/MC team as we help guys re-imagine masculinity. It’s powerful to see how the work is impacting real guys. Being able to incorporate my Latino perspective into the work has been really important to me. Throughout this process I’ve grown both personally and creatively."
        },
        {
          "last_name": "Gibson",
          "first_name": "Christina",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "I got an amazing opportunity to serve as the team lead on our newly acquired TRESemme business. From getting the business in December, to hiring our killer team in January, to pulling off a huge NYFW activation in February, to helping shape the brand purpose – it’s been a wild ride!"
        },
        {
          "last_name": "Giordano (Surbey)",
          "first_name": "Lindsey",
          "promotion_title": "Vice President, Business Development",
          "promo_blurb": "July 2017 marked 1 year in my Business Development role and year 5 at Edelman. Working on integrated pitches such as Jibo, Grey Goose and Unilever, I’ve grown to successfully manage new opportunities through the sales process; effectively packaging and monetizing Edelman’s value proposition. I like the challenge of navigating unique pitches and the ability to work with a diverse mix of colleagues in NY and across the network."
        },
        {
          "last_name": "Gunning",
          "first_name": "Stefanie",
          "promotion_title": "SVP, Group Creative Director",
          "promo_blurb": "There’s so much to be proud of here at Edelman, but for me, it comes down to people. The health + wellness Creative Network team defines what it means to bring the best of yourself to work. They are generous, kind, funny, talented, accountable, deeply engaged and always authentically themselves. We are a bunch of pirates and hobos, to be sure, and I couldn’t be happier and prouder about the company I keep."
        },
        {
          "last_name": "Hanson",
          "first_name": "Elizabeth",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "To me, the coolest thing about the digital space is that my career did not exist years ago and that it’s constantly evolving. I look forward to bringing innovation, creative and passionate thinking to our clients and work (along with earning a few more running medals) – bring it!"
        },
        {
          "last_name": "Harrison",
          "first_name": "Tory",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "Splitting my time between two groups, this past year has been full of exciting firsts. Highlights include attending and staffing clients at the World Economic Forum, placing a feature story with Fortune and partnering with Facebook to bring the first ever Facebook Live studio to the Milken Conference."
        },
        {
          "last_name": "Hart",
          "first_name": "Emily",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "For the past year, I’ve loved working on eBay and iRobot. It’s been great to work with so many other teams in 250 Hudson— and as an added bonus, the constant running between the 9th, 10th & 12th floors has no doubt prepared me well for when I eventually hike Mt. Everest."
        },
        {
          "last_name": "Heath",
          "first_name": "Taylor",
          "promotion_title": "Account Executive",
          "promo_blurb": "My PR career started at Edelman, and I’ve been lucky to work on amazing accounts. From launching Homesense, the newest home obsession, to taking on sneakerhead culture with viral Foot Locker content, this has been quite the Edel-journey. I’m looking forward to what the next year has to offer!"
        },
        {
          "last_name": "Herrmann",
          "first_name": "Brittany",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I challenged my clients to host a first of its kind ‘leg shave launch’. As a result, 85% of attendees opted for a leg shave and shared their positive experiences via social media. The event created so much buzz on and offline, it even warranted agency chatter (representing the competitive product)!"
        },
        {
          "last_name": "Hill",
          "first_name": "Jamie",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "The past year was marked by the lowest lows (exploding phones) and the highest highs (pro tip: google Samsung Twitter troll). This amazing team makes the former tolerable and the latter more rewarding than I could imagine. Truly thankful for my family at Edelman – a place like nowhere else."
        },
        {
          "last_name": "Horowitz",
          "first_name": "Brittany",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "This year has been one of the most exciting for me at Edelman! From navigating Biogen’s highly anticipated FDA approval days before Christmas to helping Cigna become known as a leader in combatting the national opioid epidemic. The results have been outstanding!"
        },
        {
          "last_name": "Hughes",
          "first_name": "Brittany",
          "promotion_title": "Senior Producer",
          "promo_blurb": "I am proud to have played a role in the integration of two awesome teams: SculpSure and Dove Real Beauty Productions. Both teams span across multiple departments inside AND outside the building. We learned to work together as one united team and made some truly fantastic work."
        },
        {
          "last_name": "Ibrahim",
          "first_name": "Reham",
          "promotion_title": "Associate Design Director",
          "promo_blurb": "This year was super awesome :) I worked on some cool projects like eBay’s 360 Block Party and Fashion Week, which gave me the chance to work with many talented illustrators. Check out the entrance and vending machine on 10 for some posters and postcards that I've designed for Edelman!"
        },
        {
          "last_name": "Ingrasin",
          "first_name": "Ryan",
          "promotion_title": "SVP, Executive Producer",
          "promo_blurb": "Any notable work from the year wouldn’t be worth mentioning without also giving credit to the great people who produced it. Like any ambitious producer, I’m only successful because I’ve been lucky enough to surround myself with the best producers in the biz. Whether it’s the Human Drone, Pinpals, TJ Maxx, or any number of reels- there’s an army of creative producers who elevate the work (and me)."
        },
        {
          "last_name": "Isaacs",
          "first_name": "David",
          "promotion_title": "VP, Executive Producer",
          "promo_blurb": "One of the many great things about my role is that I have the opportunity to collaborate with all U.S. and Global practices, and of course, Internal NY. I am very proud of the recent launch of the new resource site, Asset Library. It was a true team effort with dynamic results."
        },
        {
          "last_name": "Jankowski",
          "first_name": "Kelly",
          "promotion_title": "Executive Vice President",
          "promo_blurb": "This year has been a balancing act beyond my wildest expectations. It’s been a year that reminded me that we help clients solve real challenges, like explaining genetic-based medicine at Regeneron or advocating for universal global standards on behalf of the International Federation of Accountants. Working with an amazing, brilliant and caring set of colleagues has made it fun."
        },
        {
          "last_name": "Juarez",
          "first_name": "Justin",
          "promotion_title": "Senior Vice President, HR",
          "promo_blurb": "I’m proud to work for an organization that gives me the opportunity to work on projects impacting change in the organization but also aligns with my interests/passions. Whether it’s Diversity & Inclusion or Talent Management projects, I have a voice in shaping the company’s direction one step at a time. Working on a highly collaborative HR team who lifts each other up doesn’t hurt either."
        },
        {
          "last_name": "Kasperski",
          "first_name": "Bridgit",
          "promotion_title": "Account Executive",
          "promo_blurb": "This year, the work that made me a proud PR lady was executing another successful Food & Nutrition Conference & Expo in Boston on behalf of Unilever’s Food Scale’s “Agents of Change” campaign, dominating NYC’s Pride March with Trojan and Alexander Wang for our incredible “Protect Your Wang” collaboration, expanding my media relations by securing placements on Bustle, Refinery29, Elite Daily and Cosmopolitan and posing with model Nadia Aboulhosn. I am so excited to continue to do the best work of my life at Edelman!"
        },
        {
          "last_name": "Keh",
          "first_name": "Diana",
          "promotion_title": "VP, Project Director",
          "promo_blurb": "The past year was full of both challenges and opportunities, and the Samsung team showed itself to be extremely bright, creative and hardworking. I can't wait to see what's in store for us next!"
        },
        {
          "last_name": "Kellner",
          "first_name": "Rob",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "My account mix has been as exciting as it has been challenging - from tackling a new GCRM, counseling clients through crisis, to launching new mobile products. My clients and teammates have provided me the opportunity to grow into the Account Supervisor role and I look forward to what’s next."
        },
        {
          "last_name": "Kiernan",
          "first_name": "Callie",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I was thrilled this year to participate in my first new business pitch and to join the Regeneron EYLEA team as they prepared for a big, unbranded campaign launch, “Look to Your Future,” which has continued to evolve and embrace a more integrative comms marketing approach."
        },
        {
          "last_name": "Knox",
          "first_name": "Meaghan",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "One of my key highlights in 2017 was coordinating media engagement around Pershing’s annual INSITE conference in San Diego. Throughout INSITE, I worked on-the-ground with the team to coordinate 31 media interviews with key executives, resulting in more than 110 articles covering news from the conference."
        },
        {
          "last_name": "Kochis",
          "first_name": "Matt",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "In my first year at Edelman NY, I’m proud to have made a meaningful impact with millennial women and Gen Z guys through AXE’s efforts to combat toxic masculinity, Suave’s fake brand that showed labels don’t matter and smart influencer campaigns that reached audiences in new ways."
        },
        {
          "last_name": "Kroenlein",
          "first_name": "Evelyn",
          "promotion_title": "Senior Manager",
          "promo_blurb": "From All Staffs to ping pong tournaments and fundraisers to work showcases, I’m proud to have helped employees connect and enrich their experiences at Edelman. In FY18, I’m looking forward to my continued work with the EXD team and The Yard, and expanding my responsibilities further across Digital and the Creative Network."
        },
        {
          "last_name": "Kuklinski",
          "first_name": "Christina",
          "promotion_title": "Account Executive",
          "promo_blurb": "I am proud of my work managing the AXE sponsorship of NYFW:Men’s for its fifth and final season. This included coordinating with nine emerging designers, managing a team of stylists, securing media attendance and producing the brand’s first Instagram Story."
        },
        {
          "last_name": "La Rosa",
          "first_name": "Kristen",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I'm most proud of leveraging my integrated communications marketing skills to help plan the launch of a new, breakthrough personal care brand from Unilever, alongside my awesome, (future) award-winning team."
        },
        {
          "last_name": "Lambert",
          "first_name": "Leslie",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I am proud to have collaborated with teams across offices (D.C., Chicago, Brussels, Spain, Mexico) to provide strategic counsel to clients that are navigating challenges. In the last year, I have researched and pitched interesting angles, engaged third party expertise, and worked to understand the complex landscapes in which our clients operate."
        },
        {
          "last_name": "Lanzilli",
          "first_name": "Dan",
          "promotion_title": "Vice President",
          "promo_blurb": "Proud of our accomplishments on Speedo and AztraZeneca, reshaping those accounts for the better and establishing great, trusting client relationships through quality work and teams that really came together. We’ll soon see the launch of AztraZeneca’s Lungprint, already being called a game-changing product by doctors and patient advocacy groups. Looking forward excitedly."
        },
        {
          "last_name": "Long",
          "first_name": "Sarah",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "As a three-year Unilever vet, I aim to bring energy, passion and creativity to my work every single day – whether I’m celebrating the 60th Anniversary of the iconic Dove Beauty Bar, navigating new influencer territory, elevating Unilever’s beauty creds through insightful, dynamic programming or wrangling a bus full of beauty editors to Montauk!"
        },
        {
          "last_name": "Lowe",
          "first_name": "Meredith",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "This year, Meredith took on the role of Agents of Change Influencer Program lead for the Unilever Food Scale business. In addition to securing ten, top-tier registered dietitians influencers, she works with them daily to develop branded content for select Unilever food and refreshment brands (and gets awesome recipe tips!)."
        },
        {
          "last_name": "Maffucci",
          "first_name": "Gina",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "It was great to be a part of so many awesome teams and projects this year. I’m particularly proud of the corporate work we’ve done on PwC, where we brought Diversity & Inclusion platform to life for US CEO Tim Ryan, and Golder, which included a complete rebrand of the company, including logo, narrative and overall brand identity. Excited to see what new opportunities this fiscal year brings!"
        },
        {
          "last_name": "Mahoney",
          "first_name": "Megan",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "I’m proud to have managed Zerina Akers (stylist to Beyoncé!) to launch Dove Invisible Dry Spray and expand deodorant coverage beyond the world of beauty and onto the fashion pages of top-tier media. Another major highlight is being part of the dream team launching the brand Love, Beauty & Planet!"
        },
        {
          "last_name": "Maloney",
          "first_name": "Taylor",
          "promotion_title": "Account Executive",
          "promo_blurb": "This year, I supported a cross-border deal for General Atlantic, a global growth equity firm. The deal required close coordination with a financial communications firm based in Munich and our client in New York. We successfully executed a seamless deal announcement and received desired coverage in both New York and in Germany."
        },
        {
          "last_name": "Manzo",
          "first_name": "Mary",
          "promotion_title": "Vice President",
          "promo_blurb": "This past year at Edelman has been my most challenging yet. Taking on a role that offered many unknowns, I successfully juggled an internal embed role overseeing social for HA + CE lines of business while continuing to lead and guide our agency team."
        },
        {
          "last_name": "Marx",
          "first_name": "Dani",
          "promotion_title": "Account Executive",
          "promo_blurb": "I’m proud to work with clients who are doing impactful work to fight hunger, combat hate crimes, support women in poverty and make natural food more accessible. One moment that stands out, however, is writing press conference messaging for the President of the Center for Reproductive Rights after the election – I’ll never forget it!"
        },
        {
          "last_name": "Messinger",
          "first_name": "Hannah",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "For me FY17 was about the 4 C’s: campaigns, conferences, clients and coverage! I supported Yale University at Davos for a gallery-style cocktail reception with The Smithsonian and helped Mars Wrigley Confectionery tell its innovation story behind the Caramel M&M, a product 10 years in the making."
        },
        {
          "last_name": "Morrison",
          "first_name": "Kit",
          "promotion_title": "Senior Producer",
          "promo_blurb": "My job is to make ideas happen. This year, among other work, I produced videos, GIFs, interactive animations and more for The Home Depot, and I oversaw the creation of two full-fledged magazines—Intelligence for Flex and Agents of Change for Unilever."
        },
        {
          "last_name": "Moss",
          "first_name": "Deborah",
          "promotion_title": "SVP, Group Creative Director",
          "promo_blurb": "I’m most proud of: 1. Navigating the unique organism that is Edelman and making some things happen 2. Winning work on HomeGoods with a video that got great earned coverage 3. Creating a much-improved brand for SculpSure with an awesome integrated team."
        },
        {
          "last_name": "Murphy",
          "first_name": "Jessica",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "Moments I’m most proudest of this year: Winning Silver at the FCS Awards with OppenheimerFunds, supporting Bloomberg Philanthropies around the Mayors Challenge announcement, launching LinkedIn and Facebook digital executive positioning for Priceline Group and getting creative with Brand/Digital on Speedo USA."
        },
        {
          "last_name": "O'Connor",
          "first_name": "Kathleen",
          "promotion_title": "Vice President, Digital Health",
          "promo_blurb": "This was a great year! We have an awesome team pushing the envelope on what pharma can do on social (shout-out to the Notes to Remember team!). The most fun milestone was breaking the Guinness World Record for most online organ donor registrations in an eight hour window!"
        },
        {
          "last_name": "O'Rourke",
          "first_name": "Cari",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "There’s no doubt I was intimidated coming to Edelman from a smaller agency. Transitioning from media relations to account management and client engagement was a huge achievement and career booster for me. I’m very proud and humbled by the trusting relationships I’ve built with some of my tougher clients!"
        },
        {
          "last_name": "Okonofua",
          "first_name": "Ari",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "Working on the Flonase Sensimist Great American Roadtrip with Derek Hough and the Eh Bee family was amazing! I took the front seat in helping to orchestrate two celebrity media days and saw the incredible results from the entire activation from start to finish."
        },
        {
          "last_name": "Palazzolo",
          "first_name": "Keri",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "My first year at Edelman was a whirlwind, kicking off with my first holiday season on the eBay team. I was thrilled to bring my influencer experience to the table managing our #eBayUnboxed video series – coordinating 30+ videos with talent spanning fashion, food, sports and more to drive awareness and engagement during the crucial holiday retail timeframe."
        },
        {
          "last_name": "Papp",
          "first_name": "Lauren",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "Day-to-day client lead on digital Citi work, including redesigning Citigroup.com and the development of Brand Central, a redesigned site from Citi Global Branding for employees and partner agencies. Beefed up my website dev knowledge. Also worked on social programs for iRobot and eBay and helped lead several new business efforts."
        },
        {
          "last_name": "Paragas",
          "first_name": "Jerome",
          "promotion_title": "Contracts Specialist",
          "promo_blurb": "I feel very accomplished to know that I can be both a trusted advisor and contracts mentor to the teams within the organization.  One of my favorite milestones was when I had the recent opportunity to train the New York Consumer and Health practices on the “ins-and-outs” of client and subcontractor agreements."
        },
        {
          "last_name": "Patterson",
          "first_name": "Ashley",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "Since joining Edelman about a year ago, I've had tremendous opportunities to work on accounts that have been both challenging and exciting. From flexing my issues muscles to learning bread-and-butter PR tactics on the AZ ID team, the work I’ve done and the people I’ve worked with have encouraged my growth. I look forward to another exciting year!"
        },
        {
          "last_name": "Polanco",
          "first_name": "Charlin",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "2017 was a thrilling year.  I led the integrated Xfinity Mobile team to announce their new mobile service in April and since then have been rolling out consumer and social by design activations through each market in their foot print."
        },
        {
          "last_name": "Printz",
          "first_name": "Jaclyn",
          "promotion_title": "Vice President",
          "promo_blurb": "To celebrate Father’s Day, my Foot Locker team and I created a pop culture moment around the brand’s comical NBA Draft campaign, featuring Lonzo Ball. I also won an Eddy as a part of the Trojan team that introduced a partnership with designer Alexander Wang, encouraging NYC Pride attendees to practice safe sex and “Protect Your Wang.”"
        },
        {
          "last_name": "Rafajac",
          "first_name": "Nicole",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "Whether pitching the Elements Treehouse, sitting courtside for the #RealStrength Manifesto, leading a barber event in LA or celebrating modern fatherhood, I bring energy and creativity to Dove Men+Care every day. My passion for communications marketing has proven to be instrumental in the team’s success and only continues to grow."
        },
        {
          "last_name": "Ramirez",
          "first_name": "Penelope",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "It’s not every day that you’re able to incorporate a personal passion, in my case fashion, with your day job. Working on the TJX team allowed me to combine my love of fashion and beauty with my earned media skills. It was incredibly rewarding to help generate a great deal of coverage with Latino media."
        },
        {
          "last_name": "Redling",
          "first_name": "Marissa",
          "promotion_title": "Account Executive",
          "promo_blurb": "Having served the Dove team for more than two years, I strive to be a media maven for my team and drive value to our programming. From launching the brand's first-ever baby line, Baby Dove, to securing top tier placements in Redbook, SELF, and UsWeekly.com, I've been able to apply my learnings to the larger Unilever team while working alongside some of the brightest in the game!"
        },
        {
          "last_name": "Rincon Sato",
          "first_name": "Carolina",
          "promotion_title": "Vice President",
          "promo_blurb": "This year has been very special for both my personal and professional growth. I became a mom and helped the Baby Dove team with their multicultural outreach. Besides reveling in all things baby, I’m also thankful to have been able to execute wellness programming that benefits the Latino community with clients like Cigna and Janssen."
        },
        {
          "last_name": "Ringo",
          "first_name": "Kate",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "This year, I’ve jumped into the Samsung HA/CE account, with new set of products and stakeholders. It’s been challenging and rewarding every single day! But my favorite project has to be the Note “handraisers.” We flew 50 people to NYC after the Note7 recall not once, but twice. It was crazy and amazing to see how happy they were."
        },
        {
          "last_name": "Rishi",
          "first_name": "Aarti",
          "promotion_title": "Account Executive",
          "promo_blurb": "This year was filled with tons of milestones/projects that I am proud to have been a part of, but my most prized project was Honestly RA, an RA disease awareness campaign; the project took almost a year from start to finish, and to see it come to life after months of hard work was truly rewarding. Shameless plug: visit HonestlyRA.com to see how it turned out!"
        },
        {
          "last_name": "Rivera",
          "first_name": "Eric",
          "promotion_title": "Senior Vice President, Digital Platforms",
          "promo_blurb": "Its been a hell of a year! Some of the things I am most proud of are helping build an offering at Edelman, establish new processes, launch 6 sites, and find a truly amazing team of talented people who are passionate about the things that I am!"
        },
        {
          "last_name": "Roche",
          "first_name": "Will",
          "promotion_title": "Associate Creative Director",
          "promo_blurb": "I love that much of what we do is “Health” in name only. Like showing how everything you know about asthma is wrong (AZ Benra); being real and relatable to people living with rheumatoid arthritis (RGN’s Honestly RA); and eating the ad agencies’ lunch (Shire eyelove). Boof!"
        },
        {
          "last_name": "Roesler",
          "first_name": "Carleigh",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "In the past year, I have strategized and executed over a dozen private equity transactions for General Atlantic, including multiple cross border transactions. I am also on the front lines for Walmart’s financial communications strategy and have participated in several earnings cycles in my favorite U.S. city – Bentonville, AR."
        },
        {
          "last_name": "Rose",
          "first_name": "Sarah",
          "promotion_title": "Vice President",
          "promo_blurb": "Exploding Note7 phones, human flying drones, and a team that makes international headlines with one sassy tweet – that’s team Samsung – what’s not to be proud of. I’m so grateful for our entire Edelman family!"
        },
        {
          "last_name": "Rose",
          "first_name": "Emily",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "After joining Edelman a little over a year ago, I assisted in the development and execution of strategic communications for companies such as Wells Fargo and Credit Suisse. Currently, I am supporting venture capital firm, B Capital Group, founded by Facebook’s co-founder Eduardo Saverin."
        },
        {
          "last_name": "Rosler",
          "first_name": "Evan",
          "promotion_title": "Creative Director",
          "promo_blurb": "What a year! I grew and shaved a mustache. I lost a notebook on the 10th floor but found White Out (White Out!) on the 12th. I was also part of the team that launched XFINITY Mobile with social videos, an influencer activation, and Reggie Watts' first-ever Facebook Live performance."
        },
        {
          "last_name": "Ryan",
          "first_name": "Patrick",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "I provided communications counsel to Teekay Tankers, a tankers operator, in navigating public criticism from a shareholder activist.  I also authored investor and employee communications for several high profile transactions, including Cornell Capital’s acquisition of World Kitchen."
        },
        {
          "last_name": "Sahliyeh",
          "first_name": "Lisa",
          "promotion_title": "Senior Vice President, Recruitment",
          "promo_blurb": "Since joining Edelman, I have had the opportunity to work locally and globally, as an HR business partner and a recruitment business partner. I’m proud to have recruited and helped retain some of the best."
        },
        {
          "last_name": "Saldi",
          "first_name": "Sara",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "FY17 was full of exciting new challenges – I supported Chobani for the launch of its first non-Greek yogurt product and the graduation of its first Incubator class, guided Samsung through its unprecedented Note 7 mobile phone crisis, and helped Comcast plan for the launch of its new Financial Wellbeing employee programming."
        },
        {
          "last_name": "Sampogna",
          "first_name": "Nick",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "Whether educating media about the versatility of baking soda or promoting survey data during National Soup Month, I push myself and my teams to unearth our clients’ most compelling untold stories. This year, I’ve been proud to present writing workshops that (hopefully) help us share ideas and become even better writers."
        },
        {
          "last_name": "Samuelson",
          "first_name": "Annie",
          "promotion_title": "Account Executive",
          "promo_blurb": "This year, I supported our group’s largest transaction announcement to date, working with teams across multiple offices and practices. For global PE firm, Eurazeo, I successfully led the announcement of the launch of its consumer-driven investment division, securing a feature profile piece in The Wall Street Journal."
        },
        {
          "last_name": "Savage",
          "first_name": "Ian",
          "promotion_title": "VP, Associate Content Director",
          "promo_blurb": "I am most proud of the continued evolution of the Micro Content Studio and its team. Within this Fiscal year we partnered with every practice, resulting in ¾ of a million dollars of billable work. The MCS created awesome content strategies for Biogen, I-Robot, Home Depot, Kevzara, Lungprint and Regeneron."
        },
        {
          "last_name": "Scharf",
          "first_name": "Cara",
          "promotion_title": "Account Executive",
          "promo_blurb": "Since joining Edelman, I have grown from an enthusiastic intern to an intricately involved team member for Janssen Oncology and Corporate Health. I’m proud to have supported the DARZALEX team on multiple medical meetings and FDA approvals, and have worked hard to keep the PwC team on-track. I particularly look forward to contributing to the Janssen I&PE business, as I continue to learn and grow here at Edelman!"
        },
        {
          "last_name": "Semler",
          "first_name": "Colleen",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "Through Janssen’s Positively Fearless campaign, we empowered Black and Latinx gay and bisexual men to take charge of their health when it comes to HIV. Through relevant celebrity voices and impactful real-life stories – and our #adventuregram (@dontriskresistance) – we illustrated the risks of imperfect adherence to HIV medication and encouraged candid conversations with HCPs."
        },
        {
          "last_name": "Sharick",
          "first_name": "Kyle",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "Well, there was the time we launched viral Foot Locker spots with Tom Brady and Lonzo Ball, opened a Pop-Tarts pop-up in Times Square, and spent the day with Olympic gold medalist Simone Biles. But I’m proudest of my work with chocolate lab Merrick, Merrick Pet Care’s spokes-pup, and sharing his service to our country."
        },
        {
          "last_name": "Skansi",
          "first_name": "Meg",
          "promotion_title": "Account Executive",
          "promo_blurb": "I consider myself to be a beauty and trend-sponge, who brings creative solutions to my clients. I have served Unilever teams in various capacities from helping the Suave team hoodwink editors with a label bias campaign to whisking editors away on press trips everywhere from the Catskills to India."
        },
        {
          "last_name": "Sobota",
          "first_name": "Casey",
          "promotion_title": "Account Executive",
          "promo_blurb": "This year I was proud to support the FDA approval of Mydayis on a few different levels, including approval communications, a multi-faceted internal communications program and a consumer launch activation called Compose Your Life. I’m excited to see the months of work that have gone into the launch come to life when we debut at S.H.E. Summit this October!"
        },
        {
          "last_name": "Stevens",
          "first_name": "Rachel",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "Our first year working with the Jackson Laboratory to increase their national visibility has been fun and educational. Not only have I learned more science than I ever expected, but it’s been a wonderful opportunity to meet the CEO and introduce him to top-tier media through two very successful media days in NYC and San Francisco."
        },
        {
          "last_name": "Stuttman",
          "first_name": "Nicole",
          "promotion_title": "Account Executive",
          "promo_blurb": "In the past year I’ve worked as a member of the T.J.Maxx team to execute campaigns that drive meaning for the brand and establish strengthened relationships with media. I’m proud to have worked on the launch of The Maxx You Project – an ongoing initiative designed to inspire and enable women to let their individuality shine."
        },
        {
          "last_name": "Suchmann",
          "first_name": "Jesse",
          "promotion_title": "SVP, Executive Creative Director",
          "promo_blurb": "I'm proud to say that our team reached a new catvertising milestone this year: a piece of branded content entirely DIRECTED by a cat. Congrats to everyone who helped make this happen, the industry will never be the same."
        },
        {
          "last_name": "Sunkin",
          "first_name": "Erica",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "In the last year, I am proud of the fact that my growth has not only been evidenced through the quality of work I am able provide for my clients, but also through enhancing my role on the team via networking, sourcing new business and becoming a go-to source for FinTech and Venture Capital."
        },
        {
          "last_name": "Swymeler",
          "first_name": "Jenna",
          "promotion_title": "Senior Vice President",
          "promo_blurb": "While the last year has been challenging, I am grateful that our leadership allowed us to be  flexible and creative with problem solving. Creating an integrated “Strategy & Insights” team allowed us to truly turn data into actionable insights that were then applied to strategy across the board.  Every day I continue to be impressed by the intellect and work ethic of this team, I am most proud that I am able to stand beside them."
        },
        {
          "last_name": "Sze",
          "first_name": "Jackie",
          "promotion_title": "Senior Financial Analyst",
          "promo_blurb": "One of my major achievements was his ability to have handled finances and billing for majority of our big accounts including AstraZeneca, GlaxoSmithKline, Johnson & Johnson, Regeneron, Edgewell, Biogen, and Gilead."
        },
        {
          "last_name": "Talty",
          "first_name": "Caitlin",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I’m proud to have helped positively shift perceptions of eBay across their core audiences – whether it was through leading a successful seller recognition program, the SHINE Awards for Small Business, securing c-suite speaking engagements at events from ShopTalk to SXSW, or driving business coverage around key retail moments and market-moving announcements."
        },
        {
          "last_name": "Tendler",
          "first_name": "Chad",
          "promotion_title": "Executive Vice President",
          "promo_blurb": "This has been a professionally rewarding year, and I am proud of my work with Credit Suisse and on a string of cross-border transactions—for Vantiv, Key Safety Systems, Deere & Company, and Konica Minolta—that strengthened our M&A capabilities and expanded our collaboration across Edelman offerings and offices."
        },
        {
          "last_name": "Theccanat",
          "first_name": "Nicholas",
          "promotion_title": "Account Supervisor",
          "promo_blurb": "I am proud to continue building out Edelman’s M&A and CEO transition capabilities."
        },
        {
          "last_name": "VandenBosch",
          "first_name": "Ryan",
          "promotion_title": "Content Creative, SAE",
          "promo_blurb": "My favorite milestone this year was directing a shoot in NYC and in L.A. for The Home Depot Spring Hiring campaign. EVEN after being stranded at LAX for 2 days. Buy my favorite moments have been the brainstorms, the late nights, and the final deliveries with the best people."
        },
        {
          "last_name": "Webb",
          "first_name": "Matthew",
          "promotion_title": "Senior Designer",
          "promo_blurb": "Design advocate. Shire Compose Your Life user-interface artist. Coffee drinking muffin enthusiast. Supportive team player. Flextronics designing typeface illustrator boss. Tennessee accent talker. Idea thinker. NYC in winter lover. Tech/design geek. Occasional dancer."
        },
        {
          "last_name": "Whitmore",
          "first_name": "Aleigha",
          "promotion_title": "Senior Account Executive",
          "promo_blurb": "This year, I continued to pursue my passion for the tech and travel industries, further developing my skills and elevating client programs. I played an key role in helping Comcast successfully enter the mobile industry, and encouraged more travelers to visit Singapore and Texas through securing features in The New York Times, LA Times, ELLE and CNN (to name a few)!"
        },
        {
          "last_name": "Williams",
          "first_name": "Bart",
          "promotion_title": "VP, Executive Producer",
          "promo_blurb": "I’m proud of my contributions to help showcase and expand Edelman’s production capabilities for several clients. We took over production for the Flonase Greater American Road Trip; metrics grew nearly 300 percent over last year’s trip. We also executed complex creatives for Comcast and SculpSure to expand into event production for Ebay and Hilton."
        },
        {
          "last_name": "Wolfe",
          "first_name": "Matthew",
          "promotion_title": "HR Generalist",
          "promo_blurb": "I am proud to have been a reliable resource for my team throughout this past year while taking on the new opportunity of partnering with the NY Health and BioScience groups."
        },
        {
          "last_name": "Zabell",
          "first_name": "Melissa",
          "promotion_title": "Senior Account Supervisor",
          "promo_blurb": "I joined Edelman as a specialist in media strategy but have had the chance to learn content programming and influencer marketing. In exploring C&PA’s Influencer Marketing work, I’ve had the chance to lead successful programs for xEd, Chobani and Comcast. I’m excited to bring together an earned mindset with digital execution."
        },
        {
          "last_name": "Kahn",
          "first_name": "Jackie",
          "promotion_title": "Executive Vice President",
          "promo_blurb": "I’m most proud of our with this year with Cigna and their effort to reduce opioid use in the U.S. The Edelman team identified the issue as one that Cigna could impact, worked with the business to develop clear corporate commitments and helped sharpen their focus on solutions for the veteran community. It’s been really rewarding to work on a campaign that is making a real difference in people’s lives."
        },
        {
          "last_name": "Krupa",
          "first_name": "Jaclyn",
          "promotion_title": "Administrative Manager",
          "promo_blurb": "I’m grateful to have worked on many different tasks and projects over the past year.  I particularly enjoyed helping to run several Edelman Dialogue calls, that were attended by both Edelman colleagues and a wide variety of clients.  I also helped plan successful EMT and other meetings for the CEO/COO team.  I look forward to seeing what opportunities my new role brings."
        }
      ]
    }
  }
  
  render() {
    return (
      <div className="App-Container">
      
        <section id="App-header" className="container">
          <div className="App-header">
            <div><img id="edelman-logo" className="edelman-logo-start" src={logo} alt="edeleman-logo"/></div>
            <div><img id="level-up" src={level_up} alt="level-up"/></div>
            <div className="header-container">
              <h2 id="title" className="header-text">2017 edelman promotions</h2>
              <h3 id="description" className="header-text"> These Edelman colleagues stepped up their game all year. Day in and day out, they hustled hard and made it look easy. Just look at their faces. Damn.</h3>
            </div>
          </div>
        </section>

        <section id="App-body">

          <section id="employees" className="employees container">
            <ul className="employee-list">
              {this.state.employees.map( (employee, index) => {
                return <Employee key={index} employee={employee} handleClick={() => {this.handleClick()}}/>
              })}
            </ul>
          </section>

        </section>

        <Popup />

        <section id="popup-container" className="overlay hidden"></section>
        

      </div>

    );
  }
}

export default App;
