#!/bin/bash
cd blobs
counter = 0
for file in *.png
do
  mv $file "${file//$counter}"
  ((counter++))
done

echo All Done
