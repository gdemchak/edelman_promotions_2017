export default () => {
  return (
    <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="43.000000pt" height="43.000000pt" viewBox="0 0 43.000000 43.000000"
 preserveAspectRatio="xMidYMid meet">
      <g transform="translate(0.000000,43.000000) scale(0.100000,-0.100000)"
      fill="#000000" stroke="none">
        <path d="M0 325 l0 -105 105 0 105 0 0 105 0 105 -105 0 -105 0 0 -105z"/>
        <path d="M325 330 c55 -55 101 -100 102 -100 2 0 3 45 3 100 l0 100 -102 0
        -103 0 100 -100z"/>
        <path d="M0 100 l0 -100 102 0 103 0 -100 100 c-55 55 -101 100 -102 100 -2 0
        -3 -45 -3 -100z"/>
        <path d="M325 100 l-100 -100 103 0 102 0 0 100 c0 55 -1 100 -3 100 -1 0 -47
        -45 -102 -100z"/>
      </g>
  </svg>
  );
};
